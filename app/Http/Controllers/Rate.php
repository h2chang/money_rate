<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Service\RateService;

class Rate extends Controller
{

    public function __construct(RateService $rateService){
        $this->rateService = $rateService;
    }

    //
    public function index($currencies, $amount, $target){
        //清除空白，全部大寫
        $currencies = trim(strtoupper($currencies));
        $target = trim(strtoupper($target));

        if( !$this->rateService->check_currencies($currencies)){
            return "error in currencies";
        };

        if( !$this->rateService->check_currencies($target) && $target != -1){
            return "error in target currencies";
        };

        //清除空白，移除千分位
        $amount = trim(str_replace(',' , '', $amount));

        //數字檢查
        if ( !is_numeric($amount) || $amount<1) return "error in amount";

        return $this->rateService->calculator($currencies, $amount, $target);
        
    }
    
    public function post_change(Request $request){
        return $this->index($request->input('currencies'), $request->input('amount'), $request->input('target'));
    }
}
