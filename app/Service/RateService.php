<?php

namespace App\Service;

class RateService{
    public function calculator($currencies, $amount, $target){
        $table = json_decode(file_get_contents(storage_path('rate.json')), true);
        
        $date = [];
        foreach ($table['currencies'][$currencies] as $key => $rate) {
            if($key == $target || $target==-1 ){
                $date[$key] = number_format($amount*$rate ,2,'.',',');
            }
        }
        return $date;
    }

    public function check_currencies(string $type){
        return $type == "TWD" || $type == "USD" || $type == "JPY";
    }
}