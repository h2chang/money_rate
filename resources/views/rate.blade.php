<!DOCTYPE html>
<html>
<head>
	<title>Document</title>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
</head>
<body>
    <div class="container">
        <h1>匯率計算機</h1>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        <div class="form-control">
            <input id="TWD">
            <span>台幣 (NTD)</span>
        </div>
        
        <div class="form-control">
            <input id="JPY">
            <span>日幣 (JPY)</span>
        </div>
        
        <div class="form-control">
            <input id="USD">
            <span>美元 (USD)</span>
        </div >
        
    </div>
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>