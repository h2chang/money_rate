使用laravel8，下載後請下指令composer install，php artisan server，便可造訪http://127.0.0.1:8000/

主要程式碼在
App\Http\Controllers\Rate
App\Service\RateService

另外有寫了一點前端在resources/views/rate.blade.php

API格式為
Get: /rate/{currencies}/{amount}/{target}

Post: /change
data:{
    "currencies": 現有貨幣,
    "amount": 現有貨幣數量,
    "target": 目標貨幣(若為-1，目標貨幣為所有貨幣)
}