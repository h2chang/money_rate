$("input").change(function(){
    get_change(this.id)
});

function get_change(currencies){
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
        },
        type:"POST",
        url:"/change",
        data:{
            "currencies": currencies,
            "amount": $('#'+currencies).val(),
            "target": -1
        },
        success:function(response){
            if(typeof response != "object"){
                alert(response);
                return;
            }
            for(cur in response){
                $('#'+cur).val(response[cur]);
            }
        }
    });
}


// function get_change(currencies){
//     $.ajax({
//         headers: {
//             'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
//         },
//         type:"get",
//         url:"/rate/"+currencies+"/"+$('#'+currencies).val(),
//         success:function(response){
//             if(typeof response != "object") return;
//             for(cur in response){
//                 $('#'+cur).val(response[cur]);
//             }
//         }
//     });
// }